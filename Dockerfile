FROM openjdk:8 as build

# prepare - extract lsc
ADD lsc-core-2.1.5-dist.tar.gz .

# prepare - compile custom logger layout
ADD org org
ADD conf conf
RUN javac -classpath "lsc-2.1.5/lib/*" org/lsc/utils/output/LdifCustomLayout.java
RUN jar cvf customLayout-1.0.jar org

# runtime image
FROM gcr.io/distroless/java:8

# labels
LABEL \
    org.label-schema.name="lsc" \
    org.label-schema.description="Docker image for https://lsc-project.org" \
    org.label-schema.url="https://github.com/xxx/lsc" \
    org.label-schema.vcs-url="https://github.com/xxx/lsc" \
    org.label-schema.docker.cmd="docker run --rm -it -v $PWD/lsc.xml:/conf/lsc.xml lsc:latest"

# copy application jar
COPY --from=build lsc-2.1.5/lib /lib
COPY --from=build customLayout-1.0.jar /lib
COPY --from=build conf/logback.xml /conf/logback.xml

ENTRYPOINT ["java", "-cp", "lib/*", "org.lsc.Launcher", "--config", "/conf"]

CMD ["--clean", "all", "--synchronize", "all"]