# # LSC - LDAP Synchronization Connector
Site Officiel : https://lsc-project.org/start


Local build
```
# Set JDK8
export JAVA_HOME=`/usr/libexec/java_home -v 1.8`

# Build whole project (checkout source first)
cd /Users/nicolas/Workspace/devops/lsc
mvn -Popendj clean package

# Build custom class
javac -classpath "lib/*" org/lsc/utils/output/LdifCustomLayout.java
jar cvf customLayout-1.0.jar org
cp customLayout-1.0.jar lib/


```
